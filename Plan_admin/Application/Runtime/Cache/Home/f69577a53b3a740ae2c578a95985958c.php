<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html  xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>学员管理</title>
<link type="text/css" rel="stylesheet" href="/xplan_backend/Plan_admin/public/css/reset.css"/>
<link type="text/css" rel="stylesheet" href="/xplan_backend/Plan_admin/public/css/global.css"/>
<script type="text/javascript" src="/xplan_backend/Plan_admin/public/jquery/jquery-1.4.js"></script>
<style type="text/css">
.plus{
	background:url(/xplan_backend/Plan_admin/public/images/plus.gif) no-repeat left center;
}
.minus{
	background:url(/xplan_backend/Plan_admin/public/images/minus.gif) no-repeat left center;
}

#msg{
	width:700px;
	margin-bottom: 5px;
	color:green;
	text-align: center;
	font-size: 16px;
}
</style>
<script type="text/javascript">
$(document).ready(
	function(){
		for(var $n=1;$n<5;$n++){
			$("#div"+$n).hover(
				function (){
					$(this).css("background-color","gray");
					$(this).find("div").slideDown(300);
				},
				function (){
					$(this).css("background-color","#E4E9EC");
					$(this).find("div").slideUp(300);
				});
		}
})
function toggleItem(id){
	var menuEle=document.getElementById('menu'+id);
	var listEle=document.getElementById('list'+id);
	if (menuEle.style.display==''||menuEle.style.display=='block') {
		menuEle.style.display = 'none';
		listEle.className='plus';
	}else{
		menuEle.style.display = 'block';
		listEle.className='minus';
	}
}

function getCourse(uid){
	window.location.href="/xplan_backend/Plan_admin/index.php/home/User/getCourse/uid/"+uid;
}
 
function changeInfo(uid){
	var timeEnd=$("#paytime"+uid).val();

	window.location.href="/xplan_backend/Plan_admin/index.php/home/User/getCourseCoach/uid/"+uid+"/time_end/"+timeEnd;
} 

$(document).ready(function() {  
        var vdefault = $('#searchname').val();  
  
    $('#searchname').focus(function() {    
            if ($(this).val() == vdefault) {  
                $(this).val("");  
            }  
        });  
    $('#searchname').blur(function() {    
            if ($(this).val()== "") {  
                $(this).val(vdefault);  
            }  
        });  
});  
</script>
</head>
<body>
	  <div id="continer">
<div>
	<script type="text/javascript">
function logout(){
	  if(confirm("是否确认退出登陆？")){
		  window.location = "/xplan_backend/Plan_admin/index.php/home/Index/logout";
	  }
  }
</script>
<!--top navi-->
<div id="top-body">
		<span id="top-subject">PLAN</span>
</div>
<div id="navi">
	<div class="navi1"></div>
	<div class="navi2">
			<div class="den">
				<a href="#" onclick="logout()">退出后台</a>
			</div>
			<div id="div1">用户管理<br>
				<div class="slide">
					<a href="/xplan_backend/Plan_admin/index.php/home/User/getCoach">教练管理</a><br>
					<a href="/xplan_backend/Plan_admin/index.php/home/User/getStudent">学员管理</a><br>
				</div>
			</div>
			<div id="div2">计划管理<br>
				<div class="slide">
					<a href="">添加计划</a><br>
					<a href="/xplan_backend/Plan_admin/index.php/home/Plan/update">修改计划</a><br>
				</div>
			</div>
			<div id="div3">课程管理<br>
				<div class="slide">
					<a href="/xplan_backend/Plan_admin/index.php/home/Course/getTodayCourse">今日课程</a><br>
					<a href="/xplan_backend/Plan_admin/index.php/home/Course/getTomorrowCourse">明日课程</a><br>
					<a href="/xplan_backend/Plan_admin/index.php/home/Course/getManydayCourse">多日课程</a><br>
				</div>
			</div>
			<div id="div4">添加管理员<br>
				<div class="slide">
					<a href="/xplan_backend/Plan_admin/index.php/home/Admin/index">添加管理员</a><br>
					<a href="/xplan_backend/Plan_admin/index.php/home/Admin/update">修改管理员</a><br>
				</div>
			</div>


<!-- 			<div class="den">
	<a href="/xplan_backend/Plan_admin/index.php/home/Index/index">返回主页</a>
</div> -->
	</div>
	<div class="navi3"><?php echo ($managerTip); ?></div>
</div>
</div>
<div id="init">
		<div id="menu-body">
			<!--menu-->

	<h2><a href="#" onclick="logout()"><span>退出后台</a></span></h2>
	<h2 class="minus" id="list1" onclick="toggleItem('1')"><span>用户管理</span></h2>
	<ul id="menu1">
		<li><a href="/xplan_backend/Plan_admin/index.php/home/User/getCoach">教练管理</a></li>
		<li><a href="/xplan_backend/Plan_admin/index.php/home/User/getStudent">学员管理</a></li>
	</ul>
	<h2 class="minus" id="list2" onclick="toggleItem('2')"><span>计划管理</span></h2>
	<ul id="menu2">
		<li><a href="">添加计划</a></li>
		<li><a href="/xplan_backend/Plan_admin/index.php/home/Plan/update">修改计划</a></li>
	</ul>
	<h2 class="minus" id="list3" onclick="toggleItem('3')"><span>课程管理</span></h2>	
	<ul id="menu3">
		<li><a href="/xplan_backend/Plan_admin/index.php/home/Course/getTodayCourse">今天课程</a></li>
		<li><a href="/xplan_backend/Plan_admin/index.php/home/Course/getTomorrowCourse">明日课程</a></li>
		<li><a href="/xplan_backend/Plan_admin/index.php/home/Course/getManydayCourse">多日课程</a></li>
	</ul>	
	<h2 class="minus" id="list4" onclick="toggleItem('4')"><span>添加管理员</span></h2>
	<ul id="menu4">
		<li><a href="/xplan_backend/Plan_admin/index.php/home/Admin/index">添加管理员</a></li>
		<li><a href="/xplan_backend/Plan_admin/index.php/home/Admin/update">修改管理员</a></li>
	</ul>
	<h2><a href="/xplan_backend/Plan_admin/index.php/home/Index/index">返回主页</a></h2>

		</div>
	<div id="wel">
	<form name="form" action="/xplan_backend/Plan_admin/index.php/home/User/searchStudent" method="post">
		<div id="search">
			<span class="searchspan">搜索学员：</span>
			<input id="searchname"  name="keyword" type="text" value="请输入姓名或昵称或手机号" style="width:145px;">
			&nbsp;
			<span class="searchspan">搜索方式：</span>
			<select style="height:22px;" name="searchType">
				<option value="name">姓名</option>
				<option value="phone">手机号</option>
				<option value="nicker">昵称</option>
			</select>
			<input id="searchbtn" type="submit" value="搜索">
		</div>
		<div id="msg">点击姓名查看学员信息</div>
	</form>
	<table>
		<tr class="typeTop" align="center">
			<td>编号</td>
			<td>ID</td>
			<td>姓名</td>
			<td>性别</td>
			<td>支付时间</td>
			<td>查看课程进展</td>
			<td>修改信息</td>
			<td>查看评论</td>
		</tr>
		<?php if(is_array($student)): foreach($student as $k=>$v): if($k%2==0): $color = '#F0F4FD'; ?>
		<?php else: ?>
		<?php $color = '#FFFFFF'; endif; ?>
		<tr style="background-color:<?php echo ($color); ?>" align="center">
			<td><?php echo ($offset++); ?></td>
			<td><?php echo ($v["id"]); ?></td>
			<td><a href="/xplan_backend/Plan_admin/index.php/home/User/getUserInfo/uid/<?php echo ($v["id"]); ?>"><?php echo ($v["name"]); ?></a></td>
			<?php if($v['gender']==0): ?><td>男</td>
				<?php else: ?>
				<td>女</td><?php endif; ?>
			<td><?php echo ($v["time_end"]); ?></td>
			<td>
				<input onclick="getCourse(<?php echo ($v["id"]); ?>)" type="button" value="点击查看" style="height:22px;width:60px;">
			</td>
			<td>
			<input onclick="changeInfo(<?php echo ($v["id"]); ?>)" type="button" value="点击进入" style="height:22px;width:60px;">
			<input type="hidden" value="<?php echo ($v["time_end"]); ?>" id="paytime<?php echo ($v["id"]); ?>">
			</td>
		<td>
			<a href="#">点击查看</a>
		</td><?php endforeach; endif; ?>
		</tr>
		<?php if($count!=''): ?><tr>
			<td colspan="8" class="pages" align="center">
				<span>共<?php echo ($count); ?>个付款健友</span>
			</td>
		</tr><?php endif; ?>
		<tr>
		<td colspan="8" class="pages" align="center">
		
		<?php echo ($pageList); ?>
		</td>
			</tr>
		</table>
	</div>
	</div>
</div>
</body>
</html>